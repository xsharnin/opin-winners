variable "vpc_id" {
  description = "[Required]"
  type        = string 
  default = "vpc-d244b7b4"
}

variable "subnet_ids" {
  description = "[Required]"
  type        = list(string) 
  default = ["subnet-576ab131", "subnet-a3e83ceb", "subnet-41287d19"]
}


# #
# Stack Global Terraform Cloud variables
# Please go to https://app.terraform.io/app/waio-du-infra/workspaces
# WARNING: these variables must be set as sensitive
variable "aws_account_credentials_access_key" {
  type        = string
  description = "[Required] AWS account credentials for this account - access key (Set via Terraform Cloud)"
  default     = ""
}

variable "aws_account_credentials_access_secret_key" {
  type        = string
  description = "[Required] AWS account credentials for this account - secret key (Set via Terraform Cloud)"
  default     = ""
}

# #
# Stack Global variables
variable "aws_account_credentials_region" {
  type        = string
  description = "[Required] AWS account credentials for this account - region"
  default     = "ap-southeast-2"
}

# #
# Stack Global variables
variable "prefix" {
  type        = string
  description = "[Required] AWS resource prefix"
  default     = "12"
}