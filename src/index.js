const NodeClam = require("clamscan");

exports.handler = async function (event, context, callback) {
  let clamscan = await new NodeClam().init({
    clamdscan: {
      host: '127.0.0.1',
      port: 3310,
    }
  });

  let filePath = "C:\\Temp\\test.txt";
  const { isInfected } = await clamscan.scanFile(filePath);

  if (isInfected) {
    console.log(`The file is INFECTED with ${viruses}`);
    throw new Error('ERR_FILE_SCAN_INFECTED');
  } else {
    console.log('CLEAN');
    return 'CLEAN';
  }
};