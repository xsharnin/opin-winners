terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.25.0, < 4.0.0"
    }
  }
}

provider "aws" {
  access_key = var.aws_account_credentials_access_key
  secret_key = var.aws_account_credentials_access_secret_key
  region     = var.aws_account_credentials_region
}